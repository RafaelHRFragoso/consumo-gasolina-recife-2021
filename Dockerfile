FROM registry.gitlab.com/rafaelhrfragoso/consumo-gasolina-recife-2021:dependencies

COPY . $APP_PATH

RUN mkdir -p assets/static \
  && python manage.py collectstatic --noinput

#ENTRYPOINT ['python', '/usr/src/app/manage.py', 'runserver', '0.0.0.0:8000']
