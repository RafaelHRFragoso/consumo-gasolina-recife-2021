from django.apps import AppConfig


class ApiGasolinaConfig(AppConfig):
    name = 'api_gasolina'
